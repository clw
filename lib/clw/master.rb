# Clarkway tunnelling service
# Copyright (C) 2007 Michael Schutte
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.

# Modules
require "clw/daemon"

# External modules
require "thread"
require "socket"
require "openssl"

module Clarkway

    #
    # A master connects to a gateway and allows it to open arbitrary
    # connections.
    #
    class Master < Daemon

        attr_reader :host, :port, :gateway, :reconnect
        attr_accessor :direct_ports

        #
        # Create a master connecting to +host+:+port+, expecting the peer
        # certificate with subject +gateway+, retrying every +reconnect+
        # seconds.
        #
        def initialize(host, port, gateway, reconnect)
            super()

            @host = host
            @port = port
            @gateway = gateway
            @reconnect = reconnect
            @direct_ports = nil
        end

        protected

        #
        # Loop forever, trying to establish a gateway connection.  When
        # connected to a gateway, start regular work.
        #
        def main
            loop do
                begin
                    tcps = TCPSocket.new @host, @port
                    ssls = SSL::SSLSocket.new tcps, context
                    ssls.connect
                    ssls.sync_close = true
                rescue
                    @logger.warn("Gateway not available.")
                else
                    if ssls.peer_cert.subject.to_s != @gateway
                        @logger.warn("Possible spoofing attempt! Gateway " +
                                     "identified as #{ssls.peer_cert.subject}, " +
                                     "not starting operation.")
                    else
                        @logger.info("Gateway connection established.")
                        begin
                            do_gateway ssls
                        rescue Interrupt
                            @logger.info("Terminating gracefully.")
                            break
                        end
                    end
                ensure
                    ssls.close unless ssls.nil?
                end
                @logger.info("Retrying in #{@reconnect} seconds.")
                begin
                    sleep @reconnect
                rescue Interrupt
                    @logger.info "Terminating gracefully."
                    break
                end
            end

            stop
        end

        #
        # Stop master operations.
        #
        def stop
            @logger.info("Master terminated.")
            super
        end

        #
        # Perform control (master - gateway) connection on +ctrl+.
        #
        def do_gateway(ctrl)
            begin
                if ctrl.gets !~ /^gateway connection ready$/
                    @logger.warn "Gateway protocol mismatch."
                    ctrl.puts "sorry"
                    return
                end

                ctrl.puts "master, how can I help?"

                while line = ctrl.gets
                    case line
                    when /^thanks$/
                        ctrl.puts "always happy to serve"
                        break
                    when /^already online$/
                        break
                    when /^keepalive$/
                        ctrl.puts "okay"
                    when /^(\d+): connect me to port (\d+) on (.*)$/
                        id = $1.to_i
                        host = $3
                        port = $2.to_i
                        register_thread(id) do
                            do_connect ctrl, id, host, port
                        end
                    when /^(\d+):\ directly\ connect\ ([^:]*):(\d+)
                            \ to\ port\ (\d+)\ on\ ([^;]*);\ DN\ is\ (.*)$/x
                        id = $1
                        chost = $2
                        cport = $3.to_i
                        host = $5
                        port = $4.to_i
                        dn = $6
                        if not @direct_ports.nil? and
                                not @direct_ports.find { |range| range === cport }
                            ctrl.puts "#{$1}: sorry, direct port forbidden"
                            @logger.warn("Direct connection to #{chost}:#{cport} " +
                                         "forbidden: Port not allowed.")
                        else
                            register_thread(id) do
                                do_direct ctrl, id, chost, cport, dn, host, port
                            end
                        end
                    when /^(\d+):/
                        ctrl.puts "#{$1}: sorry"
                    else
                        ctrl.puts "sorry"
                    end
                end
            rescue
                @logger.warn("Gateway connection lost.")
            else
                @logger.warn("Gateway connection closed.")
            end
        end

        #
        # The select loop connecting Clarkway client and a TCP socket.
        #
        def select_loop(client, socket)
            loop do
                ios, dummy = select [client, socket]
                ios.each do |io|
                    target = case io
                             when socket then   client
                             else               socket
                             end
                    begin
                        data = io.sysread(BLOCKSIZE)
                    rescue EOFError
                        io.close
                        target.close
                        break
                    end
                    target.write data
                end
                break if client.closed? or socket.closed?
            end
        end

        #
        # Handle a normal connection request.
        #
        def do_connect(ctrl, id, host, port)
            @logger.info("Gateway requested connection to #{host}:#{port}.")

            # Connect to the requested TCP server
            begin
                socket = TCPSocket.new host, port
            rescue
                ctrl.puts "#{id}: connection failed"
                return
            end

            # Connect back to the gateway
            begin
                gateway = TCPSocket.new @host, @port
                gateway = SSL::SSLSocket.new gateway, context
                gateway.connect
                gateway.sync_close = true

                if gateway.peer_cert.subject.to_s != @gateway
                    @logger.warn("Possible spoofing attempt! While connecting " +
                                 "back, gateway identified as " +
                                 "#{gateway.peer_cert.subject}.")
                    return
                end

                gateway.gets
                gateway.puts "this is connection #{id}"
                @logger.info("Gateway connected to #{host}:#{port}.")
                select_loop gateway, socket
            rescue
                ctrl.puts "#{id}: connection failed"
                @logger.warn("I/O error between gateway and #{host}:#{port}.")
            ensure
                socket.close unless socket.closed?
                gateway.close unless gateway.closed?
                @logger.info("Closed connection between gateway and " +
                             "#{host}:#{port}.")
            end
        end

        #
        # Directly connect back to the client.
        #
        def do_direct(ctrl, id, chost, cport, dn, host, port)
            @logger.info("#{chost}:#{cport} requested connection to #{host}:#{port}.")

            # Connect to the Clarkway client
            begin
                tcpclient = TCPSocket.new chost, cport
                client = SSL::SSLSocket.new tcpclient, context
                client.connect
                client.sync_close = true
            rescue => e
                @logger.warn("Error on direct connection: #{e}")
                ctrl.puts "#{id}: unable to connect"
                return
            end

            if client.peer_cert.subject.to_s != dn
                client.close
                @logger.warn("Possible spoofing attempt! Wrong client ",
                             "identification on direct connection.")
                ctrl.puts "#{id}: unable to connect"
                return
            end

            ctrl.puts "#{id}: in progress"

            # Connect to the requested TCP server
            begin
                socket = TCPSocket.new host, port
            rescue => e
                client.puts "connection failed"
                client.close
                return
            end

            @logger.info("Client #{client.peer_cert.subject} " + 
                         "from #{chost}:#{cport} connected to " +
                         "#{host}:#{port}.")
            begin
                client.puts "have fun"
                select_loop client, socket
            rescue
                @logger.warn("I/O error between #{chost}:#{cport} " +
                             "and #{host}:#{port}.")
            ensure
                socket.close unless socket.closed?
                client.close unless client.closed?
                @logger.info("Closed connection to #{chost}:#{cport}.")
            end
        end

    end

end
