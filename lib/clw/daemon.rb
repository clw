# Clarkway tunnelling service
# Copyright (C) 2007 Michael Schutte
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.

# Modules
require "clw/extensions"

# External modules
require "socket"
require "openssl"

include OpenSSL

#
# Clarkway is a simple TCP forwarding service secured using SSL.  It knows the
# following modes:
#
# [master]  A master handles incoming connections and redirects all traffic
#           directly to the target hosts.  This host is part of the network
#           you want to tunnel to.  Masters are always connected to a gateway.
#
# [gateway] When Clarkway operates as gateway, its only job is to talk to the
#           master.  Clients will connect to the gateway to get access to the
#           firewalled network the master is in.
#
# [client]  A Clarkway client is connected to a gateway and opens a local port
#           for incoming connections.  The client daemon is run on the host
#           which wants to access computers in the target network.
#
# The difference between a virtual private network and Clarkway is that the
# former makes the client host a part of the target network.  If this raises a
# security issue, for example, because foreign people have access to the
# target network, Clarkway is for you.
#
module Clarkway

    VERSION = "post-1.2"
    PORT = 4354
    ID = "Clarkway #{VERSION}"
    BLOCKSIZE = 10240

    #
    # The base class for Clarkway daemons.
    #
    class Daemon

        attr_accessor :ca, :cert, :key, :crl, :logger

        #
        # Initialize the daemon.
        #
        def initialize
            @logger = @ca = @cert = @key = @crl = nil
            @threads = Hash.new
            @onstop = Array.new
            @context = nil
            @daemon = nil
            @revoked = nil
        end

        #
        # Start working, usually becoming a daemon.
        #
        def start(daemonize = true)
            @daemon = daemonize

            # Kill all threads on SIGTERM
            trap("TERM") do
                @threads.each_value do |thread|
                    thread.exit
                end
                stop
                @logger.info("Terminated gracefully.")
                exit
            end

            # Reopen log files on SIGHUP
            trap("HUP") do
                @logger.reopen
            end

            unless daemonize
                main
                return
            end

            pidpipe = IO::pipe
            fork do
                Process.setsid
                exit if fork
                Dir.chdir "/"
                pidpipe[1].puts Process.pid
                pidpipe[1].close
                STDIN.reopen "/dev/null"
                STDOUT.reopen "/dev/null", "w"
                STDERR.reopen STDOUT
                begin
                    main
                rescue => e
                    @logger.fatal(e)
                end
            end
            pid = pidpipe[0].gets.to_i
            pidpipe[0].close
            return pid
        end

        #
        # Execute +block+ on shutdown.
        #
        def on_stop(&block)
            @onstop << block
        end

        protected

        #
        # Get the SSLContext.
        #
        def context
            if @revoked.nil?
                @revoked = Hash.new
                unless @crl.nil?
                    @crl.revoked.each do |revoked|
                        @revoked[revoked.serial.to_i] = revoked.time
                    end
                end
            end
            if @context.nil?
                @context = SSL::SSLContext.new
                @context.cert = @cert
                @context.key = @key
                @context.ca_file = @ca
                @context.verify_mode = SSL::VERIFY_PEER |
                    SSL::VERIFY_FAIL_IF_NO_PEER_CERT
                @context.verify_callback = Proc.new do |ok, ctx|
                    cert = ctx.current_cert
                    if @revoked.include? cert.serial.to_i
                        raise SSL::SSLError, "#{cert.subject} revoked"
                    end
                    ok
                end
            end
            @context
        end

        #
        # Create a self-cleaning thread.
        #
        def register_thread(id, &block)
            @threads[id] = Thread.new do
                begin
                    block[]
                ensure
                    @threads.delete id
                end
            end
        end

        #
        # Enter the mainloop.
        #
        def main; end

        #
        # Shut down.
        #
        def stop
            @onstop.each do |handler|
                handler[]
            end
        end

    end

end
