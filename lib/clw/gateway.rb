# Clarkway tunnelling service
# Copyright (C) 2007 Michael Schutte
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.

# Modules
require "clw/daemon"

# External modules
require "thread"
require "monitor"
require "socket"
require "openssl"

module Clarkway

    #
    # Gateways are servers listening for master and client connections,
    # searching to provide a tunnel between them.
    #
    class Gateway < Daemon

        attr_reader :host, :port, :master
        attr_accessor :keepalive

        #
        # Create a gateway accepting connections at +host+:+port+, using
        # +master+ as the master's certificate subject.
        #
        def initialize(host, port, master)
            super()

            @host = host || "0.0.0.0"
            @port = port
            @master = master
            @online = false
            @keepalive = nil

            @connpipe = IO.pipe
            @connmap = Hash.new
            @connmap.extend MonitorMixin
            @connmap_cond = @connmap.new_cond
            @connid = 0
            @connid_lock = Mutex.new
        end

        protected

        #
        # Wait for masters and clients and handle their connections.
        #
        def main
            begin
                tcps = TCPServer.new @host, @port
            rescue => e
                @logger.fatal("Unable to listen on #{@host}:#{@port}.")
                return
            end
            @logger.info("Waiting for master on #{@host}:#{@port}.")

            loop do
                begin
                    rawclient = tcps.accept
                rescue Interrupt
                    @logger.info("Terminating gracefully.")
                    break
                rescue => e
                    @logger.warn("Error while accepting connection: #{e}.")
                    next
                end

                register_thread(rawclient) do
                    begin
                        client = OpenSSL::SSL::SSLSocket.new(rawclient, context)
                        client.sync_close = true
                        client.accept
                    rescue SSL::SSLError => e
                        @logger.warn("SSL Error while accepting connection: #{e}.")
                        client.close
                        next
                    end

                    peerhost = client.peeraddr[3]
                    peerport = client.peeraddr[1]

                    if client.peer_cert.subject.to_s == @master
                        @logger.info("Master connected from " +
                                     "#{peerhost}:#{peerport}.")
                        do_master client

                    elsif not @online
                        @logger.warn("Client #{client.peer_cert.subject} " +
                                     "(#{peerhost}:#{peerport}) ignored because " +
                                     "master is offline.")
                        client.puts "sorry, master offline"

                    else
                        @logger.info("Client #{client.peer_cert.subject} " +
                                     "connected from #{peerhost}:#{peerport}.")
                        do_client client
                    end

                    client.close
                end
            end
            
            stop
        end
        
        #
        # Stop serving.
        #
        def stop
            @logger.info("Gateway terminated.")
            super
        end

        #
        # Talk to the master.
        #
        def do_master(socket)
            socket.puts "gateway connection ready"

            case socket.gets
            when /^master, /
                if @online
                    socket.puts "already online"
                    return
                end
                @logger.info "Master has opened control connection."
                @online = true
                begin
                    do_control socket
                rescue => e
                    @logger.warn "Error in control connection: #{e}."
                ensure
                    @online = false
                    @logger.info "Control connection closed."
                end

            when /^this is connection (\d+)$/
                id = $1.to_i
                @logger.info "Master provides connection #{id}."
                thread = @connmap[id]
                @connmap.synchronize do
                    @connmap[id] = socket
                    @connmap_cond.signal
                end
                thread.join

            else
                @logger.info "Master cancelled connection."
                socket.close
            end
        end

        #
        # Perform control (master - gateway) connection on +socket+.
        #
        def do_control(socket)
            loop do
                ios, dummy = select [@connpipe[0], socket], nil, nil, @keepalive

                if ios.nil?
                    socket.puts "keepalive"
                    socket.gets
                    @logger.info "Keep-alive packet sent."
                    next
                end

                if ios.include? socket
                    line = socket.gets
                    break if line.nil?
                    if line =~ /^(\d+): (.*)$/
                        @connmap.synchronize do
                            @connmap[$1.to_i] = $2.rstrip
                            @connmap_cond.signal
                        end
                    end
                end

                if ios.include? @connpipe[0]
                    socket.write(line = @connpipe[0].gets)
                end
            end
        end

        #
        # Handle a client connection request.
        #
        def do_client(socket)
            peerhost = socket.peeraddr[3]
            peerport = socket.peeraddr[1]
            socket.puts "gateway, how can I help?"

            case line = socket.gets
            when /^connect me to port (\d+) on (.*)$/
                id = nil
                # Get a connection id
                @connid_lock.synchronize do
                    id = (@connid += 1)
                end
                @logger.info("Connection #{id}: " +
                             "#{peerhost}:#{peerport} <=> " +
                             "#{$2}:#{$1}")

                # Wait for master to open connection
                @connmap.synchronize do
                    @connmap[id] = Thread.current
                    @connpipe[1].puts "#{id}: #{line.rstrip}"
                    @connmap_cond.wait_while { @connmap[id] == Thread.current }
                end
                master = @connmap.delete(id)

                if master.is_a? String
                    # Master failed to connect (e.g. no such host)
                    socket.puts "master failed: #{master}"
                else
                    # Client may start talking
                    socket.puts "have fun"
                    loop do
                        ios, dummy = select [socket, master]
                        ios.each do |io|
                            target = case io
                                     when socket then   master
                                     else               socket
                                     end
                            begin
                                data = io.sysread(BLOCKSIZE)
                            rescue EOFError
                                io.close
                                target.close
                                break
                            end
                            target.write data
                        end
                        break if master.closed? or socket.closed?
                    end
                end

            when /^directly connect me \(([^:]+):(\d+)\) to port (\d+) on (.*)$/
                id = nil
                @connid_lock.synchronize do
                    id = (@connid += 1)
                end
                @logger.info("Connection #{id}: direct, " +
                             "#{$1}:#{$2} <=> #{$4}:#{$3}")

                # Wait for master to be ready
                @connmap.synchronize do
                    @connmap[id] = Thread.current
                    @connpipe[1].puts("#{id}: directly connect #{$1}:#{$2} " +
                                      "to port #{$3} on #{$4}; DN is " +
                                          socket.peer_cert.subject.to_s)
                    @connmap_cond.wait_while { @connmap[id] == Thread.current }
                end
                msg = @connmap.delete(id)

                if msg == "in progress"
                    socket.puts "subject is #{@master}"
                else
                    socket.puts "master failed: #{msg}"
                end
                socket.close

            else
                socket.puts "sorry"
                socket.close
            end

            @logger.info("Client #{peerhost}:#{peerport} disconnected.")
        end

    end

end
