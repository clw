# Clarkway tunnelling service
# Copyright (C) 2007 Michael Schutte
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.

# External modules
require "logger"

#
# One extension to the +Logger+ class:
#  - Allow reopening of logfiles.
#
class Logger

    class LogDevice

        #
        # Reopen the underlying logfile.
        #
        def reopen
            return if @filename.nil?
            @dev.close
            @dev = create_logfile @filename
        end

    end

    #
    # Reopen the underlying logfile.  Useful if a program like logrotate just
    # finished its work.
    #
    def reopen
        @logdev.reopen
    end

end
