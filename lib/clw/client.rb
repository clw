# Clarkway tunnelling service
# Copyright (C) 2007 Michael Schutte
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.

# Modules
require "clw/daemon"

# External modules
require "thread"
require "socket"
require "openssl"

module Clarkway

    #
    # Clients connect to gateways to get forwarded to the master.
    #
    class Client < Daemon

        attr_reader :host, :port
        attr_reader :gatehost, :gateport, :gateway
        attr_accessor :local_host, :local_port, :once, :stdio
        attr_accessor :direct_host, :direct_port
        attr_accessor :exec

        #
        # Create a client using +gatehost+:+gateport+ as gateway and +gateway+
        # as itscertificate subject. The client will be connected to
        # +host+:+port+.
        #
        # Please do not forget to set +local_host+ and +local_port+, or +stdio+,
        # depending on how you want the client to behave.
        #
        def initialize(host, port, gatehost, gateport, gateway)
            super()
            @local_host = "127.0.0.1"
            @local_port = nil
            @once = false
            @stdio = false
            @host = host
            @port = port
            @gatehost = gatehost
            @gateport = gateport
            @gateway = gateway
            @direct_host = @direct_port = @exec = nil
            @server = nil
        end

        protected

        #
        # Client operations.
        #
        def main
            begin
                if @stdio and not @daemon
                    stdio_main
                else
                    daemon_main
                end
            rescue Interrupt
                @logger.info("Terminating gracefully.")
            end
        end

        #
        # Start the client server. :-)
        #
        def daemon_main
            begin
                @server = TCPServer.new @local_host, @local_port
                @local_port = @server.addr[1]
            rescue => e
                @logger.fatal("Unable to listen on #{@local_host}:#{@local_port}.")
                return
            end
            @logger.info("Accepting connections at #{@local_host}:#{@local_port}.")

            if @once and not @exec.nil?
                execute_child
            end

            loop do
                begin
                    client = @server.accept
                rescue Interrupt
                    @logger.info("Terminating gracefully.")
                    break
                rescue => e
                    @logger.warn("Error while accepting connection: #{e}.")
                    next
                end

                if @once
                    do_client client
                    break
                else
                    register_thread(client) do
                        do_client client
                    end
                end
            end

            stop
        end

        #
        # Redirect between master and standard input/output.
        #
        def stdio_main
            if @exec.nil?
                STDOUT.sync = true
                do_client STDIN, STDOUT
            else
                execute_child
            end
        end

        #
        # Stop listening.
        #
        def stop
            @server.close unless @server.nil? or @server.closed?
            @logger.info("Client terminated.")
            super
        end

        #
        # Serve a client.
        #
        def do_client(input, output = nil)
            output = input if output.nil?

            begin
                tcps = TCPSocket.new gatehost, gateport
                ssls = SSL::SSLSocket.new tcps, context
                ssls.connect
                ssls.sync_close = true
            rescue SSL::SSLError => e
                @logger.fatal("SSL Error while connecting: #{e}.")
                return
            rescue => e
                @logger.fatal("Error while connecting: #{e}.")
                return
            end

            if ssls.peer_cert.subject.to_s != @gateway
                @logger.fatal("Possible spoofing attempt! Gateway "+
                              "identified as #{ssls.peer_cert.subject}, " +
                              "not starting operation.")
                ssls.close
                return

            elsif (line = ssls.gets) !~ /^gateway, /
                if line.nil?
                    @logger.fatal("Gateway failure.")
                else
                    @logger.fatal("Gateway message: #{line.rstrip}.")
                end
                ssls.close
                return
            end

            @logger.info("Connected to gateway #{@gatehost}:#{@gateport}.")
            if @direct_host.nil?
                master = connect_via_gateway ssls
            else
                master = connect_directly ssls
            end
            return if master.nil?

            loop do
                ios, dummy = select [input, master]
                ios.each do |io|
                    target = case io
                             when input then    master
                             else               output
                             end
                    begin
                        data = io.sysread(BLOCKSIZE)
                    rescue EOFError
                        master.close
                        output.close
                        break
                    end
                    target.write data
                end
                break if input.closed? or master.closed?
            end

            @logger.info("Connection closed.")
        end

        #
        # Normal method: Tunnel traffic through the gateway.
        #
        def connect_via_gateway(gateway)
            gateway.puts "connect me to port #{@port} on #{@host}"

            if (line = gateway.gets) != "have fun\n"
                if line.nil?
                    @logger.fatal("Gateway failure.")
                else
                    @logger.fatal("Gateway message: #{line.rstrip}.")
                end
                gateway.close
                return
            end

            @logger.info("Connection established to #{host}:#{port}.")
            gateway
        end

        #
        # Faster method: Ask master to directly connect back to the client.
        #
        def connect_directly(gateway)
            begin
                server = TCPServer.new @direct_port || 0
            rescue => e
                @logger.fatal("Unable to listen on port #{@direct_port}.")
                return
            end

            port = server.addr[1]
            @logger.info("Waiting for master on #{@direct_host}:#{port}.")
            waiter = Thread.new do
                begin
                    rawmaster = server.accept
                    master = SSL::SSLSocket.new rawmaster, context
                    master.sync_close = true
                    master.accept
                rescue => e
                    @logger.fatal("Error while accepting connection: #{e}.")
                    rawmaster.close
                    return
                end
                Thread.current["master"] = master
            end

            gateway.puts("directly connect me (#{@direct_host}:#{port})" +
                         " to port #{@port} on #{@host}")

            line = gateway.gets
            if line =~ /^subject is (.*)$/
                master_subject = $1
            else
                if line.nil?
                    @logger.fatal("Gateway failure.")
                else
                    @logger.fatal("Gateway message: #{line.rstrip}.")
                end
                gateway.close
                waiter.kill
                return
            end

            waiter.join
            master = waiter["master"]
            return if master.nil?
            if master.peer_cert.subject.to_s != master_subject
                @logger.fatal("Possible spoofing attempt! Master " +
                              "identified as #{master.peer_cert.subject}, " +
                              "not starting operation.")
                master.close
                nil
            elsif (line = master.gets) != "have fun\n"
                @logger.fatal("Master failure: #{line.rstrip}.")
                master.close
                nil
            else
                @logger.info("Connection established to #{host}:#{port}.")
                master
            end
        end

        #
        # Execute the child process.
        #
        def execute_child
            pos, command = 0, ""
            while pos < @exec.length
                char = @exec[pos].chr
                pos += 1
                if char == "%"
                    begin
                        char += @exec[pos].chr
                        pos += 1
                    rescue TypeError
                        # Nothing after %, simply leave it
                    end
                    case char
                    when "%h"
                        command += if @server.nil? then ""
                                   elsif @server.addr[3] == "0.0.0.0"
                                       "127.0.0.1"
                                   else @server.addr[3]
                                   end
                    when "%p"
                        command += if @server.nil? then ""
                                   else @server.addr[1].to_s
                                   end
                    when "%%"
                        command += "%"
                    else
                        command += char
                    end
                else
                    command += char
                end
            end

            if @stdio
                @logger.info("Attaching: #{command}.")
                file = IO::popen command, "w+"
                do_client file
            else
                @logger.info("Running: #{command}.")
                register_thread(:exec) do
                    system command
                end
            end
        end

    end

end
