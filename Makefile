#!/usr/bin/make -f

DESTDIR=
PREFIX=/usr/local
LIBDIR=$(PREFIX)/share/clarkway
BINDIR=$(PREFIX)/bin
MANDIR=$(PREFIX)/share/man/man1
MKDIR=mkdir -p

help:
	@echo "This is a ruby program which needs not be built."
	@echo "Use \`make install' to perform a local installation."

doc:
	make -C doc

install:
	install -d $(DESTDIR)$(LIBDIR) $(DESTDIR)$(BINDIR) $(DESTDIR)$(MANDIR)
	sed -e "s#\$$: << \".*\"#\$$: << \"$(LIBDIR)\"#" clw >clw.install
	install clw.install $(DESTDIR)$(BINDIR)/clw
	-rm clw.install
	cp -R lib/clw $(DESTDIR)$(LIBDIR)
	cp doc/clw.1 $(DESTDIR)$(MANDIR)/clw.1

uninstall:
	-rm $(DESTDIR)$(BINDIR)/clw
	-rm -r $(DESTDIR)$(LIBDIR)
	-rm $(DESTDIR)$(MANDIR)/clw.1

clean:
	-rm clw.install

.PHONY: help doc install uninstall clean
