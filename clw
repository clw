#!/usr/bin/env ruby
# Clarkway tunnelling service
# Copyright (C) 2007 Michael Schutte
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.

# Modules
$: << "lib"
require "clw/daemon"

# External modules
require "logger"
require "openssl"
require "yaml"

#
# Print command line usage information.
#
def usage
    puts <<EOF
Usage: clw MODE|OPTION ...
This is #{Clarkway::ID}, an SSL tunnelling application.

Modes:
    master                  Connect to a gateway and obey to its connection
                            requests.
    gateway                 Accept connections from clients and the master and
                            moderate between them.
    client                  Connect to a gateway and get forwarded to the
                            Clarkway master.
    PROFILE                 User defined profiles are stored as YAML files in
                            ~/.clw/profiles. See clw(1) for details.
    FILE                    Loads user defined profile from FILE. This only
                            works if the file name contains dots or slashes; if
                            it does not, use something like ./FILE.

Options:
    --ca FILE               Use FILE as certificate authority; mandatory.
    --cert FILE             Use FILE as certificate; mandatory.
    --key FILE              Use FILE as private key; mandatory.
    --crl FILE              Use FILE as certificate revocation list; optional.
    --subject SUBJECT       Expect peer to identify with the given certificate
                            subject; mandatory.
    --gateway HOST[:PORT]   Host and port to connect to; mandatory for master
                            and client, ignored for gateway operation.
    --reconnect N           Master: Try to reconnect to the gateway after N
                            seconds [60].
    --keepalive N           Gateway: Send keep-alive lines to avoid mysteriously
                            closing connections every N seconds [0]; a value of
                            zero disables the feature.
    --connect HOST:PORT     Host and port to connect to; mandatory for client,
                            ignored otherwise.
    --[no-]stdio            Client uses standard input and output for the
                            communication with the target.
    --listen [HOST:]PORT    Gateway: Listen on host HOST [0.0.0.0], port PORT
                                [#{Clarkway::PORT}].
                            Client: Listen on HOST [127.0.0.1], port PORT
                                [chosen randomly].
                            Master: Ignored.
    --direct HOST[:PORT]    Client: Instead of redirecting traffic through the
                            gateway, listen on PORT and tell the master to
                            connect to HOST:PORT. HOST must be this host's
                            public IP address.
    --direct-ports LIST     Master: Only directly connect to ports found in
                            LIST. Useful when a firewall is filtering ports.
    --[no-]once             Client closes server after first connection.
    --[no-]daemon           (Do not) daemonize after startup. Daemon mode is
                            enabled by default, except for clients acting in
                            stdio mode.
    --exec COMMAND          Client runs COMMAND after opening server (if
                            --listen is active) or talks to it using a pipe (in
                            --stdio mode).
    --pidfile FILE          On startup, write process ID to FILE.  If FILE
                            exists and creates a PID of a running process,
                            refuse to start.
    --logfile FILE          Write logs to FILE. Default is to disable logging if
                            daemon mode is enabled, standard error output
                            otherwise. The special value `-' logs to stderr,
                            `off' forces to disable logging.
    --loglevel LEVEL        Minimum level (info, warn, fatal) of log messages.
                            Default is to log everything (info).
    --logname NAME          Log as program NAME.  Useful if multiple instances
                            of Clarkway log into the same file.
    --help                  Show this help.

Please report bugs to Michael Schutte <m.schutte.jr@gmail.com>.
EOF
end

#
# Option parser.
#
class CommandLineParser

    class Error < RuntimeError; end

    attr_reader :args, :options

    CLWHOME = ENV["HOME"] + "/.clw"

    @@known_options = {
        "mode"          => :string,
        "ca"            => :string,
        "cert"          => :string,
        "key"           => :string,
        "crl"           => :string,
        "subject"       => :string,
        "gateway"       => :string,
        "reconnect"     => :integer,
        "keepalive"     => :integer,
        "connect"       => :string,
        "stdio"         => :boolean,
        "listen"        => :string,
        "direct"        => :string,
        "direct-ports"  => :string,
        "once"          => :boolean,
        "daemon"        => :boolean,
        "exec"          => :string,
        "pidfile"       => :string,
        "logfile"       => :string,
        "loglevel"      => :string,
        "logname"       => :string,
        "help"          => :boolean,
    }

    #
    # Create an option parser reading from +args+.
    #
    def initialize(args)
        @args = args
        @options = Hash.new
    end

    #
    # Process all options.
    #
    def process
        loop do
            arg = @args.shift
            break if arg.nil?

            case arg
            when "master", "gateway", "client"
                @options["mode"] = arg
            when /^--(no-?)?([a-zA-Z-]+)$/
                negate = (not $1.nil?)
                option = $2
                if @@known_options.include? option
                    type = @@known_options[option]

                    if negate and type != :boolean
                        raise Error, "Tried to assign boolean value to " +
                            "non-boolean option: #{arg}"
                    elsif type == :boolean
                        @options[option] = (not negate)
                    elsif type == :integer
                        value = @args.shift
                        if value.nil?
                            raise Error, "Option needs a value: #{arg}"
                        elsif value.empty?
                            @options[option] = nil
                        elsif value !~ /^\d+$/
                            raise Error, "Tried to assign string value to " +
                            "integer option: #{arg}"
                        else
                            @options[option] = value.to_i
                        end
                    elsif type == :string
                        value = @args.shift
                        if value.nil?
                            raise Error, "Option needs a value: #{arg}"
                        elsif value.empty?
                            @options[option] = nil
                        else
                            @options[option] = value
                        end
                    end
                else
                    raise Error, "Unknown option: #{arg}"
                end
            when /[.\/]/
                process_yaml arg
            else
                process_yaml CLWHOME + "/profiles/" + arg
            end
        end

        # Post-process
        @options.each_pair do |key, value|
            if value.is_a? String
                value = value.gsub(/\{(.*)\}/) {
                    case $1
                    when "open":    "{"
                    when "close":   "}"
                    when "~":       ENV["HOME"] + "/.clw"
                    when "host", "port"
                        if @options["connect"].nil?
                            ""
                        else
                            @options["connect"].split(":")[
                                ($1 == "host") ? 0 : 1
                            ].to_s
                        end
                    else            @options[key].to_s
                    end
                }
                @options[key] = value.empty? ? nil : value
            end
        end
    end

    #
    # Retrieve an option.
    #
    def [](option)
        @options[option]
    end

    #
    # Set an option manually.
    #
    def []=(option, value)
        @options[option] = value
    end

    protected

    #
    # Process a YAML file to read options from.
    #
    def process_yaml(path)
        begin
            file = File.new path
        rescue SystemCallError => e
            raise Error, "While opening profile: #{e}"
        end

        begin
            yaml = YAML::load file
        rescue => e
            raise Error, "#{path}: #{e}"
        end
        unless yaml.is_a? Hash
            raise Error, "#{path}: Hash expected"
        end

        yaml.each_pair do |key, value|
            if @@known_options.include? key
                type = @@known_options[key]

                if value.is_a? String and value.empty?
                    @options[key] = nil
                elsif type == :boolean
                    unless value == true or value == false
                        raise Error, "#{path}: #{key} is a boolean option"
                    end
                    @options[key] = value
                elsif type == :integer
                    unless value.is_a? Fixnum and value >= 0
                        raise Error, "#{path}: #{key} is a numeric option"
                    end
                    @options[key] = value
                elsif type == :string
                    @options[key] = value.to_s
                end
            end
        end

        file.close
    end

end

#
# Print an error message.
#
def error(message)
    STDERR.puts "Error: #{message}"
    STDERR.puts "Type `clw --help' for details."
end

#
# The main program.
#
def main
    options = CommandLineParser.new ARGV
    begin
        options.process
    rescue CommandLineParser::Error => e
        error "#{e}."
        return 1
    end

    if options["help"]
        usage
        return 0
    end

    mandatory = ["ca", "cert", "key", "subject"]

    case mode = options["mode"]
    when "master"
        mandatory += ["gateway"]
    when "gateway"
        # No additional mandatory options
    when "client"
        mandatory += ["gateway", "connect"]
    else
        error "Please specify a valid mode (master, gateway or client)."
        return 1
    end

    # Check options
    mandatory.each do |option|
        if options[option].nil?
            error "--#{option} is mandatory for #{mode} operation."
            return 1
        end
    end

    # Prepare the daemon
    subject = options["subject"]
    case mode
    when "master"
        ghost, gport = options["gateway"].split(":")
        if gport.nil?
            gport = Clarkway::PORT
        elsif gport !~ /^\d+$/
            error "Gateway port has to be numeric."
            return 1
        else
            gport = gport.to_i
        end

        reconnect = options["reconnect"] || 0
        reconnect = 60 if reconnect == 0

        require "clw/master"
        daemon = Clarkway::Master.new(ghost, gport,
                                      options["subject"], reconnect)

        direct_ports = options["direct-ports"]
        if direct_ports.nil? or direct_ports.empty?
            daemon.direct_ports = nil
        elsif direct_ports == "-"
            daemon.direct_ports = []
        else
            daemon.direct_ports = []
            direct_ports.split(",").each do |port|
                if port =~ /^(\d+)\.\.(\d+)$/
                    daemon.direct_ports << $1.to_i .. $2.to_i
                elsif port =~ /^(\d+)$/
                    daemon.direct_ports << $1.to_i
                else
                    error "Invalid direct port range #{port}."
                    return 1
                end
            end
        end

    when "gateway"
        listen = options["listen"]
        if listen.nil?
            lhost, lport = nil, Clarkway::PORT
        else
            lhost, lport = listen.split(":")
            lhost, lport = nil, lhost if lport.nil?
            if lport !~ /^\d+$/
                error "Local port has to be numeric."
                return 1
            end
            lport = lport.to_i
        end

        keepalive = options["keepalive"] || 0
        keepalive = nil if keepalive == 0

        require "clw/gateway"
        daemon = Clarkway::Gateway.new(lhost, lport, subject)
        daemon.keepalive = keepalive

    when "client"
        options["daemon"] = false if options["daemon"].nil?

        # Where to forward to?
        stdio = options["stdio"] || false
        if stdio
            lhost = lport = nil
        else
            listen = options["listen"] || "0"
            lhost, lport = listen.split(":")
            lhost, lport = nil, lhost if lport.nil?
            if lport !~ /^\d+$/
                error "Local port has to be numeric."
                return 1
            end
            lport = lport.to_i
            lport = nil if lport == 0
        end

        # Where to connect to?
        chost, cport = options["connect"].split(":")
        if cport.nil? or cport !~ /^\d+$/
            error "Target port has to be numeric."
            return 1
        end

        # Gateway location
        ghost, gport = options["gateway"].split(":")
        if gport.nil?
            gport = Clarkway::PORT
        elsif gport !~ /^\d+$/
            error "Gateway port has to be numeric."
            return 1
        else
            gport = gport.to_i
        end

        # Command to execute afterwards
        command = options["exec"]
        unless command.nil?
            options["once"] = true
            options["daemon"] = false
        end

        # Direct connection?
        direct = options["direct"]
        dhost = dport = nil
        unless direct.nil?
            dhost, dport = direct.split ":"
            unless dport.nil?
                if dport !~ /^\d+$/
                    error "Direct connection port has to be numeric."
                    return 1
                end
                dport = dport.to_i
                dport = nil if dport == 0
            end
        end

        require "clw/client"
        daemon = Clarkway::Client.new(chost, cport, ghost, gport, subject)
        daemon.once = options["once"] || false
        daemon.stdio = stdio
        daemon.exec = command
        daemon.local_host = lhost unless lhost.nil?
        daemon.local_port = lport
        daemon.direct_host = dhost
        daemon.direct_port = dport
        options["daemon"] = false if stdio
    end

    # SSL initialization
    cafile = daemon.ca = options["ca"]
    begin
        ca = OpenSSL::X509::Certificate.new File::read(cafile)
        daemon.cert = OpenSSL::X509::Certificate.new File::read(options["cert"])
        daemon.key = OpenSSL::PKey::RSA.new File::read(options["key"])
        unless options["crl"].nil?
            crl = OpenSSL::X509::CRL.new File::read(options["crl"])
            raise "CRL not signed by CA" unless crl.verify ca.public_key
            daemon.crl = crl
        end
    rescue => e
        error "While initializing OpenSSL: #{e}."
        return 1
    end

    # Logging
    daemonize = options["daemon"]
    daemonize = true if daemonize.nil?
    logfile = options["logfile"]
    loglevel = options["loglevel"]
    logname = options["logname"] || "clw"

    if logfile.nil?
        logfile = daemonize ? nil : STDERR
    elsif logfile == "-"
        logfile = STDERR
    elsif logfile == "off"
        logfile = nil
    end

    begin
        daemon.logger = Logger.new logfile
    rescue SystemCallError => e
        error "While opening logfile: #{e}."
        return 1
    end

    daemon.logger.progname = logname

    case loglevel
    when "fatal"
        daemon.logger.sev_threshold = Logger::FATAL
    when "warn"
        daemon.logger.sev_threshold = Logger::WARN
    else
        daemon.logger.sev_threshold = Logger::INFO
    end

    # Check PID file
    pidfile = options["pidfile"]
    unless pidfile.nil?
        begin
            oldpid = File::read(pidfile).to_i
            begin
                # Signal 0 does not kill, only check if process is alive:
                Process.kill 0, oldpid
            rescue
                # PID file outdated
            else
                error "Already running (#{pidfile}, ##{oldpid})."
                return 1
            end
        rescue
            # No old PID file
        end

        daemon.on_stop do
            begin
                File.unlink pidfile
            rescue
                # PID file probably already deleted
            end
        end
    end

    # Start the daemon
    unless pidfile.nil?
        pid = Process.pid
        pid = daemon.start true if daemonize

        begin
            file = File.new pidfile, "w"
            file.puts pid
            file.close
        rescue => e
            @logger.warn "Unable to write PID file: #{e}."
        end

        daemon.start false unless daemonize
    else
        daemon.start daemonize
    end

    # Exit successfully
    0
end

exit main if $0 == __FILE__
